using System;
using Feature.Character.CharacterBehaviour;
using Feature.ConfigLoader;
using Feature.Points;
using UnityEngine;
using UnityEngine.AI;

namespace Feature.Character.Character.Scripts.CharacterMove
{
    public class CharacterAgentMoveSystem : CharacterMoveSystem
    {
        [SerializeField] private NavMeshAgent agent;
        [SerializeField] private float minDistanceToPoint = 0.04f;

        private Vector3 _direction;

        public override void Init(CharacterEntity characterEntity, PlayerConfig playerConfig)
        {
            base.Init(characterEntity, playerConfig);
            agent.speed = playerConfig.PlayerSpeed;
        }

        protected override void CheckEndPoint()
        {
            if (agent.remainingDistance <= minDistanceToPoint)
            {
                EndMoveToPoint();
            }
        }

        protected override void UpdateMoveDirection()
        {
            agent.SetDestination(transform.position + _direction.normalized);
        }
        

        public override void MoveToPoint(PointView pointView, Action<CharacterEntity> endMoveAction = null)
        {
            base.MoveToPoint(pointView, endMoveAction);
            agent.destination = pointView.transform.position;
        }

        public override void MoveToDirection(Vector3 direction)
        {
            base.MoveToDirection(direction);
            _direction = direction;
        }
    }
}