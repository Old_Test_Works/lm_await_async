using System;
using Feature.Character.CharacterBehaviour;
using Feature.Points;
using UnityEngine;

namespace Feature.Character.Character.Scripts.CharacterMove
{
    public abstract class CharacterMoveSystem: CharacterSystem
    {
        private Action<CharacterEntity> _endMoveAction;
        private bool _isMoveToPoint;


        public override void CustomUpdate()
        {
            if (_isMoveToPoint)
            {
                CheckEndPoint();
            }
            else
            {
                UpdateMoveDirection();
            }
        }

        protected virtual void CheckEndPoint()
        {
        }
        
        protected virtual void UpdateMoveDirection()
        {
        }

        protected virtual void EndMoveToPoint()
        {
            if(_endMoveAction != null)
                _endMoveAction?.Invoke(CharacterEntity);
        }

        public virtual void MoveToPoint(PointView pointView, Action<CharacterEntity> endMoveAction = null)
        {
            _isMoveToPoint = true;
            _endMoveAction = endMoveAction;
        }
        
        public virtual void MoveToDirection(Vector3 direction)
        {
            _isMoveToPoint = false;
        }
    }
}