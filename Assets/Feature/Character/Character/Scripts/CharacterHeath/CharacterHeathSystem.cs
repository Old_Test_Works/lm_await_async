using System;
using Feature.ConfigLoader;
using UnityEngine;

namespace Feature.Character.Character.Scripts.CharacterHeath
{
    public class CharacterHeathSystem : CharacterSystem
    {
        [SerializeField] private int startHeath;
        [SerializeField] private CharacterHeathView view;
        
        public event Action OnHeathChange;
        public event Action<CharacterEntity> OnDeath;

        public int Heath => _currentHeath;
        
        
        
        private int _currentHeath = 0;
        public override void Init(CharacterEntity characterEntity, PlayerConfig config)
        {
            base.Init(characterEntity, config);
            ChangeHeath(startHeath);
        }

        public void ChangeHeath(int value)
        {
            _currentHeath += value;
            OnHeathChange?.Invoke();
            view.SetValue(_currentHeath, startHeath);
            if (_currentHeath <= 0)
            {
                OnDeath?.Invoke(CharacterEntity);
            }
        }
    }
}