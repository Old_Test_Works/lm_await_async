﻿using UnityEngine;
using UnityEngine.UI;

namespace Feature.Character.Character.Scripts.CharacterHeath
{
    public class CharacterHeathImageView:CharacterHeathView
    {
        [SerializeField] private Image image;
        
        public override void SetValue(int currentValue, int maxValue)
        {
            var value = (currentValue / (float)maxValue);
            image.fillAmount = value;
        }
    }
}