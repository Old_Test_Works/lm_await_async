using Feature.ConfigLoader;
using UnityEngine;

namespace Feature.Character.Character.Scripts
{
    public abstract class CharacterSystem : MonoBehaviour
    {
        public CharacterEntity CharacterEntity => _characterEntity;

        public PlayerConfig PlayerConfig => _playerConfig;
        
        private CharacterEntity _characterEntity;

        private PlayerConfig _playerConfig;
        
        public virtual void Init(CharacterEntity characterEntity, PlayerConfig playerConfig)
        {
            _characterEntity = characterEntity;
            _playerConfig = playerConfig;
        }

        public virtual void CustomUpdate()
        {
        }
    }
}