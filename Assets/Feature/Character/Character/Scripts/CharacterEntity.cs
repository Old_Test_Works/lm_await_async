using System.Collections.Generic;
using Feature.Character.Character.Scripts.CharacterHeath;
using Feature.Character.Character.Scripts.CharacterMove;
using Feature.ConfigLoader;
using UnityEngine;

namespace Feature.Character.Character.Scripts
{
    public class CharacterEntity: MonoBehaviour
    {
        [SerializeField] private CharacterMoveSystem _characterMoveSystem;
        [SerializeField]  private CharacterHeathSystem _characterHeathSystem;

        public int Id => _id;
        public CharacterMoveSystem CharacterMoveSystem => _characterMoveSystem;
        public CharacterHeathSystem CharacterHeathSystem => _characterHeathSystem;

        private List<CharacterSystem> _systems;
        private int _id;

        public void Init(int id, PlayerConfig config)
        {
            _id = id;
            _systems = new List<CharacterSystem>();
            
            _systems.Add(_characterMoveSystem);
            _systems.Add(_characterHeathSystem);

            foreach (var system in _systems)
            {
                system.Init(this, config);   
            }
        }

        private void Update()
        {
            if (_systems != null)
            {
                foreach (var system in _systems)
                {
                    system.CustomUpdate();
                }
            }
        }
        

    }
}