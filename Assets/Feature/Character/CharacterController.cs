﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Feature.Architecture;
using Feature.Camera;
using Feature.Character.Character.Scripts;
using Feature.Character.CharacterBehaviour;
using Feature.ConfigLoader;
using Feature.Points;
using Feature.Tools;
using UnityEngine;

namespace Feature.Character
{
    public class CharacterController : Controller
    {

        [SerializeField] private CharacterEntity prefabCharacter;

        public IReadOnlyList<CharacterEntity> Characters => _characters.Values.ToList();
        
        
        private PointsController PointsController => Manager.GetController<PointsController>();
        private CharacterBehaviorController BehaviorController => Manager.GetController<CharacterBehaviorController>();
        private ConfigLoaderController ConfigLoaderController => Manager.GetController<ConfigLoaderController>();

        

        private int _lastId = 0;
        
        private Dictionary<int,CharacterEntity> _characters = new Dictionary<int, CharacterEntity>();

        [ContextMenu("SpawnCharacter")]
        public async Task<CharacterEntity> SpawnCharacterAsync()
        {
            var config = await ConfigLoaderController.LoadConfigAsync<PlayerConfig>();

                
            var spawnPoint = PointsController.Points.Where(x => x.PointType == PointType.SpawnPoint).GetRandomValue();

            var entity = Instantiate(prefabCharacter, spawnPoint.transform.position, Quaternion.identity, transform);
            entity.Init(_lastId, config);
            
            _characters.Add(_lastId, entity);
            _lastId++;
            entity.CharacterHeathSystem.OnDeath +=  DeSpawnCharacter;
            return await Task.FromResult(entity);
        }

        private void DeSpawnCharacter(CharacterEntity entity)
        {
            if (_characters.ContainsKey(entity.Id))
                _characters.Remove(entity.Id);
            
            BehaviorController.RemoveCharacter(entity);
            
            Destroy(entity.gameObject);
        }
    }
}