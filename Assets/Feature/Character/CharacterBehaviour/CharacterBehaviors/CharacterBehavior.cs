﻿using Feature.Character.Character.Scripts;
using Feature.Points;
using Feature.Tools;
using UnityEngine;

namespace Feature.Character.CharacterBehaviour.CharacterBehaviors
{
    //TODO сделал MonoBehaviour чтобы  вешать настройки на него

    public enum CharacterBehaviorType
    {
        Npc,
        Player,
    }

    public abstract class CharacterBehavior : MonoBehaviour
    {
        [SerializeField] private CharacterBehaviorType characterBehaviorType;

        public CharacterBehaviorType CharacterBehaviorType => characterBehaviorType;
        
        protected CharacterBehaviorController CharacterBehaviorController => _characterBehaviorController;
        private CharacterBehaviorController _characterBehaviorController;
        
        public virtual void Init(CharacterBehaviorController characterBehaviorController)
        {
            _characterBehaviorController = characterBehaviorController;
        }

        public virtual void AddCharacter(CharacterEntity characterEntity)
        {
        }

        public virtual void RemoveCharacter(CharacterEntity characterEntity)
        {
        }
    }
}