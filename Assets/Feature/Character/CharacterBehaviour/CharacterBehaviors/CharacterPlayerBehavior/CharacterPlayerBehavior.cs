﻿using System;
using System.Collections.Generic;
using Feature.Character.Character.Scripts;
using Feature.Joystick;
using UnityEngine;

namespace Feature.Character.CharacterBehaviour.CharacterBehaviors.CharacterPlayerBehavior
{
    public class CharacterPlayerBehavior : CharacterBehavior
    {
        private Dictionary<int,CharacterEntity> _characters = new Dictionary<int, CharacterEntity>();
        

        public override void AddCharacter(CharacterEntity characterEntity)
        {
            if(!_characters.ContainsKey(characterEntity.Id))
                _characters.Add(characterEntity.Id, characterEntity);
            
            CharacterBehaviorController.CameraController.SetTargetObject(characterEntity.transform);
        }
        
        public override void RemoveCharacter(CharacterEntity characterEntity)
        {
            if(_characters.ContainsKey(characterEntity.Id))
                _characters.Remove(characterEntity.Id);
        }

        private void Update()
        {
            if (CharacterBehaviorController != null)
            {
                var vector = CharacterBehaviorController.JoystickController.GetDirection();
                Vector3 direction = new Vector3(vector.x, 0, vector.y);
                foreach (var characterEntity in _characters.Values)
                {
                    characterEntity.CharacterMoveSystem.MoveToDirection(direction);
                }  
            }
        }
    }
}