﻿using System;
using System.Collections.Generic;
using Feature.Character.Character.Scripts;
using Feature.Points;
using Feature.Tools;
using UnityEngine;

namespace Feature.Character.CharacterBehaviour.CharacterBehaviors
{
    
    public class CharacterGroupBehavior : CharacterBehavior
    {
        [SerializeField] private float timeWait = 1f;
        
        private Dictionary<int,CharacterEntity> _characters = new Dictionary<int, CharacterEntity>();

        private PointView _currentPoint;

        private float _timeToNewPoint = 0;
        
        
        public override void AddCharacter(CharacterEntity characterEntity)
        {
            if(!_characters.ContainsKey(characterEntity.Id))
                _characters.Add(characterEntity.Id, characterEntity);

            if (_currentPoint == null)
            {
                GenerateNextPoint();
            }
            
            characterEntity.CharacterMoveSystem.MoveToPoint(_currentPoint);
        }


        public override void RemoveCharacter(CharacterEntity characterEntity)
        {
            if(_characters.ContainsKey(characterEntity.Id))
                _characters.Remove(characterEntity.Id);
        }

        private void Update()
        {
            if (_currentPoint != null)
            {
                _timeToNewPoint -= Time.deltaTime;
                if (_timeToNewPoint <= 0)
                {
                    EndCycle();
                }
            }
        }

        private void EndCycle()
        {
            GenerateNextPoint();
            foreach (var character in _characters)
            {
                character.Value.CharacterMoveSystem.MoveToPoint(_currentPoint);
            }
        }

        private void GenerateNextPoint()
        {
            _currentPoint =  CharacterBehaviorController.GetRandomPoint();
            _timeToNewPoint = timeWait;
        }
    }
}