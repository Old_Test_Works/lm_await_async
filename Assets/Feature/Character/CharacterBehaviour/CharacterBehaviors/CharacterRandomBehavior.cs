﻿using Feature.Character.Character.Scripts;
using Feature.Points;
using UnityEngine;


namespace Feature.Character.CharacterBehaviour.CharacterBehaviors
{
    public class CharacterRandomBehavior : CharacterBehavior
    {
        public override void AddCharacter(CharacterEntity characterEntity)
        {
            MoveToNextPoint(characterEntity);
        }

        private void MoveToNextPoint(CharacterEntity characterEntity)
        {
            var nextPoint =  CharacterBehaviorController.GetRandomPoint();

            characterEntity.CharacterMoveSystem.MoveToPoint(nextPoint, MoveToNextPoint);
        }
    }
}