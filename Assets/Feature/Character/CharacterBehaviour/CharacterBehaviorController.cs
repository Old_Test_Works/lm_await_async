﻿using System;
using System.Collections.Generic;
using System.Linq;
using Feature.Architecture;
using Feature.Camera;
using Feature.Character.Character.Scripts;
using Feature.Character.CharacterBehaviour.CharacterBehaviors;
using Feature.Joystick;
using Feature.Points;
using Feature.Tools;
using UnityEngine;

namespace Feature.Character.CharacterBehaviour
{
    public class CharacterBehaviorController : Controller
    {
        
        [SerializeField] private List<CharacterBehavior> characterBehavior;



        public PointsController PointsController {
            get
            {
                if(_pointsController == null)
                    _pointsController = Manager.GetController<PointsController>();
                return _pointsController;
            }
        }
        
        public JoystickController JoystickController {
            get
            {
                if(_joystickController == null)
                    _joystickController = Manager.GetController<JoystickController>();
                return _joystickController;
            }
        }
        
        public CameraController CameraController => Manager.GetController<CameraController>();
        
        
        private JoystickController _joystickController;
        private PointsController _pointsController;

        private Dictionary<int, CharacterBehavior> _beh = new Dictionary<int, CharacterBehavior>();

        public override void Init()
        {
            base.Init();
            foreach (var behavior in characterBehavior)
            {
                behavior.Init(this);
            }
        }
        
        public void SetPlayerBehaviorInCharacter(CharacterEntity entity)
        {
            var playerBex = characterBehavior.Where(x=>x.CharacterBehaviorType == CharacterBehaviorType.Player).GetRandomValue();
            playerBex.AddCharacter(entity);

            _beh.Add(entity.Id, playerBex);
        }
        

        public void SetRandomNpcBehaviorInCharacter(CharacterEntity view)
        {
            var randomBeh = characterBehavior.Where(x=>x.CharacterBehaviorType == CharacterBehaviorType.Npc).GetRandomValue();
            randomBeh.AddCharacter(view);

            _beh.Add(view.Id, randomBeh);
        }

        public void RemoveCharacter(CharacterEntity view)
        {
            if (_beh.ContainsKey(view.Id))
                _beh[view.Id].RemoveCharacter(view);
        }

        public PointView GetRandomPoint(Predicate<PointView> p = null)
        {
            if (p == null)
                return PointsController.Points.GetRandomValue();

            return PointsController.Points.Where(x => p(x)).GetRandomValue();
        }
        
    }
}