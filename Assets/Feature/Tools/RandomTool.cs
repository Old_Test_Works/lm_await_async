using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Feature.Tools
{
    public static class RandomTool
    {
        public static T GetRandomValue<T>(this IReadOnlyList<T> list)
        {
            
            return list == null || list.Count == 0 ? default : list[Random.Range(0, list.Count)];
        }
        
        public static T GetRandomValue<T>(this IEnumerable<T> list)
        {

            return list.ToList().GetRandomValue();
        }
    }
}