﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Feature.Architecture;
using UnityEngine;

namespace Feature.ConfigLoader
{
    public class ConfigLoaderController : Controller
    {
        [SerializeField] private MiniGameConfig miniGameConfig;
        [SerializeField] private PlayerConfig playerConfig;
        [SerializeField] private InteractionConfig interactionConfig;
        
        private List<Config> _configs;

        private Dictionary<Type, Config> _configsLoaded = new Dictionary<Type, Config>();
        public override void Init()
        {
            _configs = new List<Config>();
            _configs.Add(miniGameConfig);
            _configs.Add(playerConfig);
            _configs.Add(interactionConfig);
            base.Init();
        }

        public Task<T> LoadConfigAsync<T>() where T: Config
        {
            var type = typeof(T);
            if (_configsLoaded.ContainsKey(type))
                return Task.FromResult(_configsLoaded[type] as T);

            Thread.Sleep(1000);

            var config = _configs.FirstOrDefault(x => x.GetType() == typeof(T));
            _configsLoaded.Add(type, config);
            
            
            return Task.FromResult(config as T);
        }
    }


    [Serializable]
    public class Config
    {
    }

    [Serializable]
    public class PlayerConfig : Config
    {
        [SerializeField] private float playerSpeed = 5;
        
        public float PlayerSpeed => playerSpeed;
    }

    [Serializable]
    public class InteractionConfig : Config
    {
        //todo лень выносить в отдельный конфиг
        [SerializeField] private int countInteraction = 3;
        
        [SerializeField] private float sizeInteraction =1;
        [SerializeField] private float timeInteraction = 3;
        [SerializeField] private float timeStopInteraction = 6;
        [SerializeField] private float probabilityMiniGame = 0.4f;
        
        public int CountInteraction => countInteraction;
        public float SizeInteraction => sizeInteraction;
        public float TimeInteraction => timeInteraction;
        public float TimeStopInteraction => timeStopInteraction;
        public float ProbabilityMiniGame => probabilityMiniGame;
        
    }

    [Serializable]
    public class MiniGameConfig : Config
    {
        [SerializeField] private float speedMiniGame = 1;
        [SerializeField] private float sizeGoodZone;
        
        public float SpeedMiniGame => speedMiniGame;
        public float SizeGoodZone => sizeGoodZone;
    }
}