using System.Collections.Generic;
using Feature.Architecture;
using Feature.Tools;

namespace Feature.Points
{
    public class PointsController: Controller
    {
        public IReadOnlyList<PointView> Points => _points;

        private List<PointView> _points = new List<PointView>();
        
        public void InitPoint(PointView pointView)
        {
            _points.Add(pointView);
            var a = Points.GetRandomValue();
        }

    }
}