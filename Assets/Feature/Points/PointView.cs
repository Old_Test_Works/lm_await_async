using Feature.Architecture;
using UnityEngine;

namespace Feature.Points
{
    public enum PointType
    {
        None,
        SpawnPoint,
        Interaction
        
    }

    public class PointView: UIController<PointsController>
    {
        [SerializeField] private PointType _pointType;
        public PointType PointType => _pointType;
        
        private void Start()
        {
            Controller.InitPoint(this);
        }
    }
}