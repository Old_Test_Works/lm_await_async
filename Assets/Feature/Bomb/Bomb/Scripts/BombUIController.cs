﻿using System.Collections.Generic;
using Feature.Architecture;
using Feature.Bomb.Bomb.Scripts.Bombs;
using Feature.Tools;
using UnityEngine;

namespace Feature.Bomb.Bomb.Scripts
{
    public class BombUIController : UIController<BombController>
    {

        [SerializeField] private Transform bombContent;
        
        [SerializeField] private List<BombBehaviour> bombPrefabs;

        protected override void Subscribe()
        {
            base.Subscribe();
            
            Controller.OnSpawnBomb += SpawnBomb;
        }

        protected override void Unsubscribe()
        {
            base.Unsubscribe();
            
            Controller.OnSpawnBomb -= SpawnBomb;
        }
        

        private void SpawnBomb(Vector3 pos)
        {
            var bomb = bombPrefabs.GetRandomValue();
            var view = Instantiate(bomb, pos, Quaternion.identity, bombContent);
            view.Init();
        }
    }
}