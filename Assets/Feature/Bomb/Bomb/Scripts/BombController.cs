﻿using System;
using Feature.Architecture;
using Feature.TapSystem;
using UnityEngine;

namespace Feature.Bomb.Bomb.Scripts
{
    public class BombController : Controller
    {
        private TapController TapController => Manager.GetController<TapController>();
        
        
        public event Action<Vector3> OnSpawnBomb;



        private void OnEnable()
        {
            TapController.OnClick += ClickReaction;
        }

        private void OnDisable()
        {
            TapController.OnClick -= ClickReaction;
        }
        
        private void ClickReaction()
        {
            if(TapController.Position != null)
                OnSpawnBomb?.Invoke(TapController.Position.Value);
        }
    }
}