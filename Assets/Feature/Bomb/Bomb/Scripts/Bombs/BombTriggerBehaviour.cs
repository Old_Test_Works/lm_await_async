﻿using System;
using UnityEngine;

namespace Feature.Bomb.Bomb.Scripts.Bombs
{
    public class BombTriggerBehaviour : BombBehaviour
    {
        private void OnCollisionEnter(Collision collision)
        {
            StartExplosion();
        }
        
    }
}