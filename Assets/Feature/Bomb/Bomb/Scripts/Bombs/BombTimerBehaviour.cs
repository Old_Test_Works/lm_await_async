﻿using UnityEngine;

namespace Feature.Bomb.Bomb.Scripts.Bombs
{
    public class BombTimerBehaviour : BombBehaviour
    {
        
        [SerializeField] private float timeToExplosion = 1f;
        
        private float _time;

        public override void Init()
        {
            base.Init();
            _time = timeToExplosion;
        }
        
        private void Update()
        {
            if (_time >= 0)
            {
                _time -= Time.deltaTime;
                if (_time <= 0)
                {
                    StartExplosion();
                }
            }
        }

        
    }
}