﻿using Feature.Bomb.DamageZone;
using Feature.Bomb.DamageZone.DamageZoneBehaviour;
using UnityEngine;

namespace Feature.Bomb.Bomb.Scripts.Bombs
{
    public abstract class BombBehaviour : MonoBehaviour
    {
        [SerializeField] private Vector3 deltaPos = new Vector3(0,5,0);
      
        [SerializeField] private GameObject bombView;
        [SerializeField] private DamageZoneUIController damageUIController;
  
        public virtual void Init()
        {
            bombView.transform.position += deltaPos;
        }

        protected virtual void StartExplosion()
        {
            damageUIController.Damage();
            Destroy(this.gameObject);
        }

        
    }
}