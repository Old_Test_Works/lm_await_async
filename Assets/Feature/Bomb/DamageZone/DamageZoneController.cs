﻿using System;
using System.Collections.Generic;
using Feature.Architecture;
using Feature.Character.Character.Scripts;
using UnityEngine;
using CharacterController = Feature.Character.CharacterController;

namespace Feature.Bomb.DamageZone
{
    public class DamageZoneController : Controller
    {
        private CharacterController CharacterController => Manager.GetController<CharacterController>();

        private List<CharacterEntity> _buffer = new List<CharacterEntity>();


        

        public void AddDamageCharacters(GameObject deadZoneCollider, float radius, int damage)
        {
            _buffer.Clear();
            _buffer.AddRange(CharacterController.Characters);
            foreach (var character in _buffer)
            {
                var startPos = character.transform.position;
                var endPos = deadZoneCollider.transform.position;
                //Debug.DrawRay(startPos, endPos - startPos, Color.red);
                
                Ray ray = new Ray(startPos, endPos-startPos);
                if (Physics.Raycast(ray, out var hit, 100))
                {
                    if(hit.collider == null)
                        continue;
                    if (hit.collider.gameObject == deadZoneCollider)
                        character.CharacterHeathSystem.ChangeHeath(-damage);
                }
            }
        }
    }
}