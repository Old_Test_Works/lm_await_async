﻿using Feature.Architecture;
using UnityEngine;

namespace Feature.Bomb.DamageZone.DamageZoneBehaviour
{
    public class DamageZoneUIController : UIController<DamageZoneController>
    {
        [SerializeField] private float radius;
        [SerializeField] private int damage;
        [SerializeField] private GameObject colliderDeadZone;
        
        
        public virtual void Damage()
        {
            Controller.AddDamageCharacters(colliderDeadZone,radius,damage);
        }

    }
}