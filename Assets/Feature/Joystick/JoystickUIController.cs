﻿using System;
using Feature.Architecture;
using UnityEngine;

namespace Feature.Joystick
{
    
    public interface IJoystickView
    {
        public Vector2 Direction { get; }
    }
    
    public class JoystickUIController : UIController<JoystickController>
    {
        [SerializeField] private GameObject joystickView;

        private IJoystickView _joystickView;
        protected override void Subscribe()
        {
            base.Subscribe();
            //todo не люблю GetComponent, но это самый быстрый/фбстрактный способ
            _joystickView = joystickView.GetComponent<IJoystickView>();
            Controller.SetJoystickView(_joystickView);
        }
        
    }
}