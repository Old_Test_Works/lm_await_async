﻿using Feature.Architecture;
using UnityEngine;

namespace Feature.Joystick
{
    public class JoystickController : Controller
    {
        private IJoystickView _joystickView;
        private Vector3 _default = new Vector3(0, 0, 0);
        
        public void SetJoystickView(IJoystickView joystickView)
        {
            _joystickView = joystickView;
        }

        public Vector2 GetDirection()
        {
            if (_joystickView == null)
                return _default;
            return _joystickView.Direction;
        }

    }
}