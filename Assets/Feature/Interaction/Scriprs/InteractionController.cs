﻿
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Feature.Architecture;
using Feature.ConfigLoader;
using Feature.Points;
using Feature.Tools;
using UnityEngine;
using CharacterController = Feature.Character.CharacterController;

namespace Feature.Interaction.Scriprs
{
    public class InteractionController:Controller
    {
        [SerializeField] private InteractionPointView interactionPointViewPrefab;
        
        private ConfigLoaderController ConfigLoaderController => Manager.GetController<ConfigLoaderController>();
        private PointsController PointsController => Manager.GetController<PointsController>();

        private List<InteractionPointView> _interactionPoint = new List<InteractionPointView>();

        private InteractionConfig _config;
        private CharacterController CharacterController {
            get
            {
                if (_characterController == null)
                    _characterController = Manager.GetController<CharacterController>();
                
                return _characterController;
                
            }
        }

        private CharacterController _characterController;

        public IReadOnlyList<InteractionPointView> InteractionPointView => _interactionPoint;
        
        public async Task SpawnInitializePointsAsync()
        {
            _interactionPoint = new List<InteractionPointView>();
            _config =  await ConfigLoaderController.LoadConfigAsync<InteractionConfig>();
            var points = PointsController.Points.Where(x => x.PointType == PointType.Interaction).ToList();
            var randomPoint = new List<PointView>();
            for (int i = 0; i < _config.CountInteraction; i++)
            {
                var point = points.GetRandomValue();
                if(point == null)
                    break;
                
                randomPoint.Add(point);
                points.Remove(point);
            }

            foreach (var pointView in randomPoint)
            {
                var transformPoint = pointView.transform;
                var view = Instantiate(interactionPointViewPrefab, transformPoint.position, transformPoint.rotation, this.transform);
                view.Init(pointView, _config);
                _interactionPoint.Add(view);
            }
            
            
        }

        private void Update()
        {
            foreach (var pointView in _interactionPoint)
            {
                if(pointView.IsComplete)
                    continue;

                var charactersInRadius = CharacterController.Characters
                    .Count(x => Vector3.Distance(x.transform.position, pointView.transform.position) <= _config.SizeInteraction);
                
                if (charactersInRadius == 0)
                    charactersInRadius = -1;

                pointView.AddProgress(charactersInRadius*Time.deltaTime);
            }
            
            
            
        }
    }
}