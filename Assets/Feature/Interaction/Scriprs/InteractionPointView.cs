﻿using Feature.ConfigLoader;
using Feature.Points;
using UnityEngine;

namespace Feature.Interaction.Scriprs
{
    public class InteractionPointView : MonoBehaviour
    {
        [SerializeField] private GameObject radius;
        
        [SerializeField] private GameObject progress;

        [SerializeField] private float minY = 0;
        [SerializeField] private float maxY = 1.5f;

        public bool IsComplete => _isComplete;
        private bool _isComplete;

        private float _progress;
        
        private PointView _pointView;
        private InteractionConfig _config;
        
        public void Init(PointView pointView, InteractionConfig config)
        {
            _config = config;
            radius.transform.localScale =
                new Vector3(config.SizeInteraction, config.SizeInteraction, config.SizeInteraction);

            _pointView = pointView;

            
            Redraw(0);
        }

        //todo по хорошем сделать отдельный progress view
        private void Redraw(float current)
        {
            var y = current / _config.TimeInteraction;
            y = Mathf.Lerp(minY ,maxY, y);
            var position = _pointView.transform.position;
            progress.transform.position = new Vector3(position.x, y, position.z);
        }

        //todo как же тупо хранить эту логику на view, потом бы переделать чтобы контроллер работает с entity, а view росто отображает данные с него
        public void AddProgress(float charactersInRadius)
        {
            _progress += charactersInRadius;
            
            //_progress = Mathf.Clamp(_progress, 0, _config.TimeInteraction);
            
            if (_progress <= 0)
                _progress = 0;
            
            if (_progress >= _config.TimeInteraction)
            {
                _progress = _config.TimeInteraction;
                _isComplete = true;
            }

            Redraw(_progress);

        }
    }
}