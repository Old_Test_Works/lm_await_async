﻿using Feature.Architecture;
using UnityEngine;

namespace Feature.Camera
{
    public class CameraUIController: UIController<CameraController>
    {
        [SerializeField] private GameObject camera;
        
        protected override void Subscribe()
        {
            base.Subscribe();
            Controller.SetCamera(camera);
        }
    }
}