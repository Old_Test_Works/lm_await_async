﻿using System;
using Feature.Architecture;
using UnityEngine;

namespace Feature.Camera
{
    public class CameraController : Controller
    {
        private Vector3 _deltaPos;
        private GameObject _camera;
        private Transform _targetPos;

        public void SetCamera(GameObject camera)
        {
            _deltaPos = camera.transform.position;
            _camera = camera;
        }

        public void SetTargetObject(Transform pos)
        {
            _targetPos = pos;
        }

        private void Update()
        {
            if (_camera && _targetPos)
            {
                _camera.transform.position = _targetPos.position + _deltaPos;
            }
        }
    }
}