﻿using System;
using Feature.Architecture;
using UnityEngine;
using UnityEngine.UI;

namespace Feature.Level.Scripts
{
    public class LevelUIController : UIController<LevelController>
    {

        [SerializeField] private GameObject bg;
        [SerializeField] private Button spawnLevelButton;


        protected override void Subscribe()
        {
            base.Subscribe();
            Controller.OnLevelSpawn += ShowUI;
            spawnLevelButton.onClick.AddListener(ClickSpawnLevelReaction);
        }



        protected override void Unsubscribe()
        {
            base.Unsubscribe();
            Controller.OnLevelSpawn -= ShowUI;
            
            spawnLevelButton.onClick.RemoveListener(ClickSpawnLevelReaction);
        }


        private void ClickSpawnLevelReaction()
        {
            Controller.SpawnLevelAsync();
            spawnLevelButton.gameObject.SetActive(false);
        }
        private void ShowUI()
        {
            bg.SetActive(false);
        }

    }
}