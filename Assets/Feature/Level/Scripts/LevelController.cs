﻿using System;
using Feature.Architecture;
using Feature.Character.CharacterBehaviour;
using Feature.Interaction.Scriprs;
using UnityEngine;
using CharacterController = Feature.Character.CharacterController;

namespace Feature.Level.Scripts
{
    public class LevelController : Controller
    {
        [SerializeField] private int countPlayer = 20;

        public event Action OnLevelSpawn;
        
        private CharacterController CharacterController => Manager.GetController<CharacterController>();
        private CharacterBehaviorController BehaviorController => Manager.GetController<CharacterBehaviorController>();

        private InteractionController InteractionController => Manager.GetController<InteractionController>();

        public async void SpawnLevelAsync()
        {
            await InteractionController.SpawnInitializePointsAsync();
            
            for (int i = 0; i < countPlayer; i++)
            {
                var character = await CharacterController.SpawnCharacterAsync();
                BehaviorController.SetRandomNpcBehaviorInCharacter(character);
            }
            
            var player = await CharacterController.SpawnCharacterAsync();
            BehaviorController.SetPlayerBehaviorInCharacter(player);
            OnLevelSpawn?.Invoke();
        }
    }
}