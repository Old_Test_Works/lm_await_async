﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Feature.Architecture
{
    //Todo я уж не стал выстраивать полноценнную архитктуру
    //понятно что тут должен быть сначала сбор контроллеров, а потом uiсонтроллеров отложная иницилизация и тд
    //но для тестового достаточно)))
    [DefaultExecutionOrder(-100)]
    public class ControllerManager : MonoBehaviour
    {
        public static ControllerManager Manager => _manager;
        private static ControllerManager _manager;

        private Dictionary<Type, Controller> _controllers = new Dictionary<Type, Controller>();

        private void Awake()
        {
            _manager = this;
            var controllers = GetComponentsInChildren<Controller>();
            foreach (var controller in controllers)
            {
                AddController(controller);
            }

            foreach (var controller in controllers)
            {
                controller.Init();
            }
        }

        public void AddController(Controller controller)
        {
            var type = controller.GetType();
            
            Debug.Log($"<color=green>Add controller {type.Name} </color>");
            _controllers.Add(type, controller);
            
        }

        public TController GetController<TController>() where TController : Controller
        {
            var type = typeof(TController);
            Debug.Log($"<color=orange>Get controller {type.Name} </color>");
            
            if (_controllers.ContainsKey(type))
                return (TController) _controllers[type];
            
 
            Debug.Log($"<color=red>Not found controller {type.Name} </color>");
            return null;
        }
    }
}