﻿using System.Threading.Tasks;
using Feature.Joystick;
using UnityEngine;

namespace Feature.Architecture
{
    [DefaultExecutionOrder(-90)]
    public abstract class Controller: MonoBehaviour
    {
        protected ControllerManager Manager => ControllerManager.Manager;


        private void Awake()
        {
            Init();
        }

        public virtual void Init()
        {
            //Manager.AddController(this);
        }
        

    }
}