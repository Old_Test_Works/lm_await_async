﻿using System;
using UnityEngine;

namespace Feature.Architecture
{
    [DefaultExecutionOrder(-80)]
    public abstract class UIController<TController>: MonoBehaviour where TController : Controller
    {
        private ControllerManager Manager => ControllerManager.Manager;
        
        protected TController Controller;

        private void OnEnable()
        {
            Controller = Manager.GetController<TController>();
            Subscribe();
        }

        private void OnDisable()
        {
            Unsubscribe();
        }

        protected virtual void Subscribe()
        {
        }

        protected virtual void Unsubscribe()
        {
        }
        
    }
}