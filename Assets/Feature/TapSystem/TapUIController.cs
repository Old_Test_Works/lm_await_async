﻿using Feature.Architecture;
using UnityEngine;

namespace Feature.TapSystem
{
    public class TapUIController : UIController<TapController>
    {

        [SerializeField] private GameObject view;
    
        private void Update()
        {
            if (Controller.Position == null)
            {
                if(view.activeSelf)
                    view.SetActive(false);
                return;
            }
            
            if(!view.activeSelf)
                view.SetActive(true);
            view.transform.position = Controller.Position.Value;
        }
        
    }
}