﻿using System;
using Feature.Architecture;
using UnityEngine;

namespace Feature.TapSystem
{
    public class TapController : Controller
    {
        public event Action OnClick;
        
        //todo возможно есть смысл переделать на класс а не структурку
        public Vector3? Position => _position;


        private Vector3? _position;
        

        private void Update()
        {
            //UpdateMousePosition();
            //UpdateMouseClick();
        }

        /*
        private void UpdateMousePosition()
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 100))
            {
                _position = hit.point;
            }
            else
            {
                _position = null;
            } 
        }

        private void UpdateMouseClick()
        {
            if (Input.GetMouseButtonDown(0))
            {
                OnClick?.Invoke();
            }
        }
        */
    }
}